/*
 * Copyright 2019 Edson Contreras
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 *******************************************************************************
 * i2c_util.c :
 * This file contains the functions associated to the i2c connection and
 * communication. It is based on the content of classic.c of Teck1.
 *******************************************************************************
 */
#include <assert.h>
#include <fcntl.h>
// #include <linux/i2c.h> // Uncomment this line for Fedora build.
#include <linux/i2c-dev.h>
#include <linux/uinput.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "classic.h"

// ============================================================================
/*
 * External functions :
 */
// ============================================================================
extern void timed_wait(long sec, long usec, long early_usec);

// ============================================================================
/*
 * Global file descriptor for i2c communication
 */
// ============================================================================
static int i2c_fd = -1;

// ============================================================================
/*
 * Open I2C bus and check capabilities :
 */
// ============================================================================
void i2c_init(const char *node) {
  unsigned long i2c_funcs = 0; /* Support flags */
  int rc;

  i2c_fd = open(node, O_RDWR); /* Open driver /dev/i2s-1 */
  if (i2c_fd < 0) {
    perror("Opening /dev/i2s-1");
    puts("Check that the i2c-dev & i2c-bcm2708 kernel modules "
         "are loaded.");
    abort();
  }

  /*
   * Make sure the driver supports plain I2C I/O:
   */
  rc = ioctl(i2c_fd, I2C_FUNCS, &i2c_funcs);
  assert(rc >= 0);
  assert(i2c_funcs & I2C_FUNC_I2C);
}

// ============================================================================
/*
 * Close the I2C driver :
 */
// ============================================================================
void i2c_close(void) {
  close(i2c_fd);
  i2c_fd = -1;
}

// ============================================================================
/*
 * Configure the nunchuk/controller for no encryption:
 * Works with all controller types.
 */
// ============================================================================
void controller_init() {
  static char init_msg1[] = {0xF0, 0x55};
  static char init_msg2[] = {0xFB, 0x00};
  struct i2c_rdwr_ioctl_data msgset;
  struct i2c_msg iomsgs[1];
  int rc;

  iomsgs[0].addr = 0x52;     /* Address of Nunchuk */
  iomsgs[0].flags = 0;       /* Write */
  iomsgs[0].buf = init_msg1; /* Nunchuk 2 byte sequence */
  iomsgs[0].len = 2;         /* 2 bytes */

  msgset.msgs = iomsgs;
  msgset.nmsgs = 1;

  rc = ioctl(i2c_fd, I2C_RDWR, &msgset);
  assert(rc == 1);

  timed_wait(0, 200, 0); /* Nunchuk needs time */

  iomsgs[0].addr = 0x52;     /* Address of Nunchuk */
  iomsgs[0].flags = 0;       /* Write */
  iomsgs[0].buf = init_msg2; /* Nunchuk 2 byte sequence */
  iomsgs[0].len = 2;         /* 2 bytes */

  msgset.msgs = iomsgs;
  msgset.nmsgs = 1;

  rc = ioctl(i2c_fd, I2C_RDWR, &msgset);
  assert(rc == 1);
}

// ============================================================================
/*
 * Read classic controller data:
 */
// ============================================================================
int classic_read(classic_s *data) {
  struct i2c_rdwr_ioctl_data msgset;
  struct i2c_msg iomsgs[1];
  char zero[1] = {0x00}; /* Written byte */
  int rc;

  timed_wait(0, 15000, 0);

  /*
   * Write the controller register address of 0x00 :
   */
  iomsgs[0].addr = 0x52; /* controller address */
  iomsgs[0].flags = 0;   /* Write */
  iomsgs[0].buf = zero;  /* Sending buf */
  iomsgs[0].len = 1;     /*  bytes */

  msgset.msgs = iomsgs;
  msgset.nmsgs = 1;

  rc = ioctl(i2c_fd, I2C_RDWR, &msgset);
  if (rc < 0)
    return -1; /* I/O error */

  timed_wait(0, 200, 0); /* Zzzz, controller needs time */

  /*
   * Read 6 bytes starting at 0x00 :
   */
  iomsgs[0].addr = 0x52;             /* controller address */
  iomsgs[0].flags = I2C_M_RD;        /* Read */
  iomsgs[0].buf = (char *)data->raw; /* Receive raw bytes here */
  iomsgs[0].len = 6;                 /* 6 bytes */

  msgset.msgs = iomsgs;
  msgset.nmsgs = 1;

  rc = ioctl(i2c_fd, I2C_RDWR, &msgset);
  if (rc < 0)
    return -1; /* Failed */

  data->P[RX] = ((data->raw[0] & 0xC0) >> 3) | ((data->raw[1] & 0xC0) >> 5) |
                ((data->raw[2] & 0x80) >> 7);
  data->P[RY] = data->raw[2] & 0x1F;
  data->P[LX] = data->raw[0] & 0x3F;
  data->P[LY] = data->raw[1] & 0x3F;
  data->P[RT] = data->raw[3] & 0x1F;
  data->P[LT] = ((data->raw[2] & 0x60) >> 2) | ((data->raw[3] & 0xE0) >> 5);

  data->B[BDR] = data->raw[4] & 0x80 ? 0 : 1;
  data->B[BDD] = data->raw[4] & 0x40 ? 0 : 1;
  data->B[BLT] = data->raw[4] & 0x20 ? 0 : 1;
  data->B[BM] = data->raw[4] & 0x10 ? 0 : 1;
  data->B[BH] = data->raw[4] & 0x08 ? 0 : 1;
  data->B[BP] = data->raw[4] & 0x04 ? 0 : 1;
  data->B[BRT] = data->raw[4] & 0x02 ? 0 : 1;

  data->B[BZL] = data->raw[5] & 0x80 ? 0 : 1;
  data->B[BB] = data->raw[5] & 0x40 ? 0 : 1;
  data->B[BY] = data->raw[5] & 0x20 ? 0 : 1;
  data->B[BA] = data->raw[5] & 0x10 ? 0 : 1;
  data->B[BX] = data->raw[5] & 0x08 ? 0 : 1;
  data->B[BZR] = data->raw[5] & 0x04 ? 0 : 1;
  data->B[BDL] = data->raw[5] & 0x02 ? 0 : 1;
  data->B[BDU] = data->raw[5] & 0x01 ? 0 : 1;

  return 0;
}
