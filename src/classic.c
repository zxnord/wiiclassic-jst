/*
 * Copyright 2019 Edson Contreras
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*********************************************************************
 * classic.c :
 *  Teck1 work was modified in order to send uinput events like real
 *  gamepad. In addition to that, also both analog pots were added
 *  with absolute axes changes. The driver project was restructured
 *  in to a cmake project. Timed_wait.c file was kept with no changes
 *  because it is being used as it is.
 *********************************************************************
 * Based on classic.c - by Teck1 :
 *  Read events from Nintendo Classic controller via
 *  Raspberry PI I2C connection,  and replicate button presses as a
 *  virtual keyboard (also reads joystick movement, but not yet
 *  extended for key presses or mouse movement)
 *********************************************************************
 * Based on: nunchuk.c - by Warren Gay
 * From: Mastering the Raspberry Pi - ISBN13: 978-1-484201-82-4
 * Reuse here is per Warren's disclaimer:
 *  "This source code is placed into the public domain."
 *********************************************************************/

#include <signal.h>
#include <stdio.h>
#include <string.h>

#include "classic.h"

// ============================================================================
/*
 * I2C auxiliary functions:
 */
// ============================================================================
extern void i2c_init(const char *node);
extern void i2c_close(void);
extern void controller_init();
extern int classic_read(classic_s *data);

// ============================================================================
/*
 * uinput auxiliary functions:
 */
// ============================================================================
extern void uinput_close(int fd);
extern int uinput_open();
extern void uinput_syn(int fd);

// ============================================================================
/*
 * Utilitary functions:
 */
// ============================================================================
extern int buttonChange(classic_s *data, classic_s *last);
extern void performButtonChange(int fd, int f_debug, classic_s *cl_data,
                                classic_s *cl_last);
extern void performLeftStickChange(int fd, int f_debug, classic_s *cl_data);
extern void performRightStickChange(int fd, int f_debug, classic_s *cl_data);
extern int stickChange(classic_s *data, classic_s *last, int from, int to);
extern void copy_classic_s(classic_s *now, classic_s *last);
extern void dump_data_classic(classic_s *data, classic_s *last);
extern void sigint_handler(int signo);
extern void setAnalogInitialValues(analog_p initLeftVal, analog_p initRightVal);

// ============================================================================
/*
 * Local variables:
 */
// ============================================================================
static int f_debug = 0; /* True to print debug messages */

// ============================================================================
/*
 * Main program :
 */
// ============================================================================
int main(int argc, char **argv) {
  int fd, need_sync;
  int init = 1;
  int rel_x = 0, rel_y = 0;
  analog_p an_left0, an_right0;
  classic_s cl_data, cl_last;
  int delta_count = 0, n, key;

  if (argc > 1 && !strcmp(argv[1], "-d"))
    f_debug = 1; /* Enable debug messages */

  i2c_init("/dev/i2c-3"); /* Open I2C controller */
  controller_init();      /* Turn off encryption */

  signal(SIGINT, sigint_handler); /* Trap on SIGINT */
  fd = uinput_open();             /* Open /dev/uinput */

  classic_read(&cl_data); /* Read to set initial values */
  copy_classic_s(&cl_data, &cl_last);

  an_right0.raw_data[0] = cl_data.P[0];
  an_right0.raw_data[1] = cl_data.P[1];
  an_left0.raw_data[0] = cl_data.P[2];
  an_left0.raw_data[1] = cl_data.P[3];

  an_left0.x_pos = 0;
  an_left0.y_pos = 0;
  an_right0.x_pos = 0;
  an_right0.y_pos = 0;

  setAnalogInitialValues(an_left0, an_right0);

  while (!is_signaled) {

    copy_classic_s(&cl_data, &cl_last);

    while (!is_signaled & (classic_read(&cl_data) < 0)) {
    }

    // chack for button or analog pots change
    if (buttonChange(&cl_data, &cl_last)) {
      delta_count++;
      need_sync = 1;
      performButtonChange(fd, f_debug, &cl_data, &cl_last);
    } else if (stickChange(&cl_data, &cl_last, 0, 2)) {
      delta_count++;
      need_sync = 1;
      performRightStickChange(fd, f_debug, &cl_data);
    } else if (stickChange(&cl_data, &cl_last, 2, 4)) {
      delta_count++;
      need_sync = 1;
      performLeftStickChange(fd, f_debug, &cl_data);
    } else
      continue;

    if (need_sync) {
      if (f_debug) {
        printf("delta: %i\n", delta_count);
        dump_data_classic(&cl_data, &cl_last); /* Dump nunchuk data */
      }

      uinput_syn(fd);
      need_sync = 0;
    }
  }

  putchar('\n');
  uinput_close(fd);
  i2c_close();

  return 0;
}
