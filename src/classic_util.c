/*
 * Copyright 2019 Edson Contreras
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 ******************************************************************************
 * classic_util.c :
 * This file contains utilitary functions for main routine. Here is where the
 * changes of buttons and anaglog pots are detected. Also has functions for
 * debugging curve correction and memory copying.
 ******************************************************************************
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "classic.h"

// ============================================================================
/*
 * UInput external functions :
 */
// ============================================================================
extern void uinput_postkey(int fd, unsigned int key, unsigned int dir);
extern void uinput_left_movement(int fd, int x, int y);
extern void uinput_right_movement(int fd, int x, int y);

// ============================================================================
/*
 * Global variables :
 */
// ============================================================================
int is_signaled = 0; /* Exit program if signaled */
static analog_p an_left0, an_right0, an_lastLeft, an_lastRight;

const char *const classic_button_names[classic_buttons_MAX + 1] = {
    "BDR", "BDL", "BDU", "BDD", "BLT", "BRT", "BM", "BH",
    "BP",  "BZL", "BZR", "BA",  "BB",  "BX",  "BY"};

const char *const classic_pot_names[classic_pots_MAX + 1] = {"RX", "RY", "LX",
                                                             "LY", "LT", "RT"};

// ============================================================================
/*
 * Copy classic_s structure :
 */
// ============================================================================
void copy_classic_s(classic_s *now, classic_s *last) {
  // copy current data over to last
  int n = sizeof(now->raw) / sizeof(now->raw[0]);
  memcpy(last->raw, now->raw, n);

  n = sizeof(now->P) / sizeof(now->P[0]);
  memcpy(last->P, now->P, n);

  n = sizeof(now->B) / sizeof(now->B[0]);
  memcpy(last->B, now->B, n);
}

// ============================================================================
/*
 * Dump the classic controller data (Only in debug -d option):
 */
// ============================================================================
void dump_data_classic(classic_s *data, classic_s *last) {
  int x;

  printf("Raw : ");
  for (x = 0; x < 6; x++)
    printf(" [%02X]", data->raw[x]);
  putchar('\n');
  printf("Last: ");
  for (x = 0; x < 6; x++)
    printf(" [%02X]", last->raw[x]);
  putchar('\n');
  printf("      data last\n");
  for (x = 0; x < classic_pots_MAX + 1; x++)
    printf("%3s = %04X %04X\n", classic_pot_names[x], data->P[x], last->P[x]);

  for (x = 0; x < classic_buttons_MAX + 1; x++)
    printf("%3s = %04X %04X\n", classic_button_names[x], data->B[x],
           last->B[x]);
}

// ============================================================================
/*
 * Signal handler to quit the program :
 */
// ============================================================================
void sigint_handler(int signo) { is_signaled = 1; /* Signal to exit program */ }

// ============================================================================
/*
 * Adjust right analog pot value to detect the percentage of pot position.
 * X-axis :
 * 0    - Center
 * 100  - Right Max
 * -100 - Left Max
 *
 * Y-axis :
 * 0    - Center
 * 100  - Up Max
 * -100 - Down Max
 */
// ============================================================================
static int rightCurve(int relxy, int sign, unsigned int pos) {
  int val = relxy;

  if (sign) {
    val -= (int)an_right0.raw_data[pos];
    return (int)(val * 20.0 / 3.0);
  } else {
    return -1 * (val * -20.0 / 3.0 + 100);
  }
}

// ============================================================================
/*
 * Adjust left analog pot value to detect the percentage of pot position.
 * X-axis :
 * 0    - Center
 * 100  - Right Max
 * -100 - Left Max
 *
 * Y-axis :
 * 0    - Center
 * 100  - Up Max
 * -100 - Down Max
 */
// ============================================================================
static int leftCurve(int relxy, int sign, unsigned int pos) {
  int val = relxy;

  if (sign) {
    val -= (int)an_left0.raw_data[pos];
    return (int)(val * 100.0 / 31.0);
  } else {
    return -1 * (val * -100.0 / 31.0 + 100);
  }
}

// ============================================================================
/*
 * Checks the RAW field against last measured values to see if a
 * button was pressed
 * return 0 if no change
 *        1 if changed
 */
// ============================================================================
int buttonChange(classic_s *data, classic_s *last) {
  if (memcmp(&(data->raw[4]), &(last->raw[4]), 2) == 0)
    return 0;
  else
    return 1;
}

// ============================================================================
/*
 * Checks the RAW field against last measured for stick changes,
 * with 2 points wiggle room for changes
 */
// ============================================================================
int stickChange(classic_s *data, classic_s *last, int from, int to) {
  int n;

  for (n = from; n < to; n++) {
    if (abs(data->P[n] - last->P[n]) > 0)
      return 1;
  }

  return 0;
}

// ============================================================================
/*
 * Detect if button change is pressed or released, then call
 * uinput event change :
 */
// ============================================================================
void performButtonChange(int fd, int f_debug, classic_s *cl_data,
                         classic_s *cl_last) {
  int key, n;

  // Button logic is (dataB-lastB), if 0, no action happened. If 1, button
  // down, if -1 button up
  for (n = 0; n < (sizeof(cl_data->B) / sizeof(cl_data->B[0])); n++) {

    if (cl_data->B[n] - cl_last->B[n]) // if not 0, a key action happened
    {
      // if 1, then button down, if -1 it's a button release
      if ((cl_data->B[n] - cl_last->B[n]) == 1)
        key = PKEY_DOWN;
      else
        key = PKEY_UP;

      cl_last->B[n] = cl_data->B[n];
      uinput_postkey(fd, classic_button_map[n], key);

      if (f_debug)
        printf("%3s %i\n", classic_button_names[n], key);
    }
  }
}

// ============================================================================
/*
 * After first i2c analog pots read, save initial values for later check
 * the absolute value change :
 */
// ============================================================================
void setAnalogInitialValues(analog_p initLeftVal, analog_p initRightVal) {
  an_left0.raw_data[0] = initLeftVal.raw_data[0];
  an_left0.raw_data[1] = initLeftVal.raw_data[1];

  an_right0.raw_data[0] = initRightVal.raw_data[0];
  an_right0.raw_data[1] = initRightVal.raw_data[1];

  an_left0.x_pos = initLeftVal.x_pos;
  an_left0.y_pos = initLeftVal.y_pos;

  an_right0.x_pos = initRightVal.x_pos;
  an_right0.y_pos = initRightVal.y_pos;

  an_lastLeft = an_left0;
  an_lastRight = an_right0;
}

// ============================================================================
/*
 * Detect if left pot change was in Y-axis or X-axis and send the uinput
 * event afterwards :
 */
// ============================================================================
void performLeftStickChange(int fd, int f_debug, classic_s *cl_data) {
  analog_p value;
  int sign_x, sign_y;

  value.raw_data[0] = cl_data->P[2];
  value.raw_data[1] = cl_data->P[3];

  sign_x = value.raw_data[0] & an_left0.raw_data[0];
  sign_y = value.raw_data[1] & an_left0.raw_data[1];

  value.x_pos = leftCurve((int)value.raw_data[0], sign_x, 0);
  value.y_pos = leftCurve((int)value.raw_data[1], sign_y, 1);

  if ((value.x_pos != an_lastLeft.x_pos) ||
      (value.y_pos != an_lastLeft.y_pos)) {
    uinput_left_movement(fd, value.x_pos, value.y_pos);

    an_lastLeft.raw_data[0] = value.raw_data[0];
    an_lastLeft.raw_data[1] = value.raw_data[1];
    an_lastLeft.x_pos = value.x_pos;
    an_lastLeft.y_pos = value.y_pos;

    if (f_debug) {
      printf("New analog position: <%d, %d>\n", value.x_pos, value.y_pos);
    }
  }
}

// ============================================================================
/*
 * Detect if left pot change was in Y-axis or X-axis and send the uinput
 * event afterwards :
 */
// ============================================================================
void performRightStickChange(int fd, int f_debug, classic_s *cl_data) {
  analog_p value;
  int sign_x, sign_y;

  value.raw_data[0] = cl_data->P[0];
  value.raw_data[1] = cl_data->P[1];

  sign_x = value.raw_data[0] & an_right0.raw_data[0];
  sign_y = value.raw_data[1] & an_right0.raw_data[1];

  value.x_pos = rightCurve((int)value.raw_data[0], sign_x, 0);
  value.y_pos = rightCurve((int)value.raw_data[1], sign_y, 1);

  if ((value.x_pos != an_lastRight.x_pos) ||
      (value.y_pos != an_lastRight.y_pos)) {
    uinput_right_movement(fd, value.x_pos, value.y_pos);

    an_lastRight.raw_data[0] = value.raw_data[0];
    an_lastRight.raw_data[1] = value.raw_data[1];
    an_lastRight.x_pos = value.x_pos;
    an_lastRight.y_pos = value.y_pos;

    if (f_debug) {
      printf("New analog position: <%d, %d>\n", value.x_pos, value.y_pos);
    }
  }
}