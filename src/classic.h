/*
 * Copyright 2019 Edson Contreras
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 ******************************************************************************
 * classic.h :
 * Header file that defines several convinient enums, structs and shared
 * variables used by most files.
 ******************************************************************************
 */
#ifndef _WII_CLASSIC_H_
#define _WII_CLASSIC_H_

// ============================================================================
/*
 * Convenient enums :
 */
// ============================================================================
enum classic_pots { RX, RY, LX, LY, LT, RT, classic_pots_MAX = RT };

enum classic_buttons {
  BDR,
  BDL,
  BDU,
  BDD,
  BLT,
  BRT,
  BM,
  BH,
  BP,
  BZL,
  BZR,
  BA,
  BB,
  BX,
  BY,
  classic_buttons_MAX = BY
};

enum postkey { PKEY_UP, PKEY_DOWN };

// ============================================================================
/*
 * Convenient structures :
 */
// ============================================================================
typedef struct {
  unsigned char P[6];   // Pot Array
  unsigned char B[15];  // Button Array
  unsigned char raw[6]; // Raw received data
} classic_s;

typedef struct a_pot {
  unsigned char raw_data[2];
  int x_pos;
  int y_pos;
} analog_p;

// ============================================================================
/*
 * Shared variables :
 */
// ============================================================================
extern unsigned int classic_button_map[];
extern int is_signaled; /* Exit program if signaled */

#endif // _WII_CLASSIC_H_