/*
 * Copyright 2019 Edson Contreras
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 ******************************************************************************
 * uinput_util.c :
 * This file contains the functions associated to the creation, definition and
 * system calls for uinput events. Here is defined the "virtual gamepad" that
 * is detected by OS.
 ******************************************************************************
 */
#include <assert.h>
#include <fcntl.h>
#include <linux/joystick.h>
#include <linux/uinput.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "classic.h"

// ============================================================================
/*
 * Button events array :
 */
// ============================================================================
unsigned int classic_button_map[classic_buttons_MAX + 1] = {
    BTN_DPAD_RIGHT, BTN_DPAD_LEFT, BTN_DPAD_UP, BTN_DPAD_DOWN, BTN_TL,
    BTN_TR,         BTN_SELECT,    BTN_MODE,    BTN_START,     BTN_TL2,
    BTN_TR2,        BTN_A,         BTN_B,       BTN_X,         BTN_Y};

// ============================================================================
/*
 * Create a new uinput device :
 */
// ============================================================================
int uinput_open() {
  int fd;
  struct uinput_user_dev uinp;
  int rc, n;

  fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
  if (fd < 0) {
    perror("Opening /dev/uinput");
    exit(1);
  }

  // Basic Events.
  rc = ioctl(fd, UI_SET_EVBIT, EV_KEY);
  assert(!rc);

  for (n = 0; n < classic_buttons_MAX + 1; n++) {
    ioctl(fd, UI_SET_KEYBIT, classic_button_map[n]);
  }

  rc = ioctl(fd, UI_SET_EVBIT, EV_ABS);
  assert(!rc);

  // Left analog pad
  rc = ioctl(fd, UI_SET_ABSBIT, ABS_X);
  assert(!rc);
  rc = ioctl(fd, UI_SET_ABSBIT, ABS_Y);
  assert(!rc);

  // Right analog pad
  rc = ioctl(fd, UI_SET_ABSBIT, ABS_RX);
  assert(!rc);
  rc = ioctl(fd, UI_SET_ABSBIT, ABS_RY);
  assert(!rc);

  memset(&uinp, 0, sizeof uinp);
  strncpy(uinp.name, "Simple Gamepad", UINPUT_MAX_NAME_SIZE);
  uinp.id.bustype = BUS_USB;
  uinp.id.vendor = 0x3;
  uinp.id.product = 0x3;
  uinp.id.version = 2;

  uinp.absmax[ABS_X] = 100; // Parameters of thumbsticks
  uinp.absmin[ABS_X] = -100;
  uinp.absfuzz[ABS_X] = 0;
  uinp.absflat[ABS_X] = 6;

  uinp.absmax[ABS_Y] = 100; // Parameters of thumbsticks
  uinp.absmin[ABS_Y] = -100;
  uinp.absfuzz[ABS_Y] = 0;
  uinp.absflat[ABS_Y] = 6;

  uinp.absmax[ABS_RX] = 100; // Parameters of thumbsticks
  uinp.absmin[ABS_RX] = -100;
  uinp.absfuzz[ABS_RX] = 0;
  uinp.absflat[ABS_RX] = 6;

  uinp.absmax[ABS_RY] = 100; // Parameters of thumbsticks
  uinp.absmin[ABS_RY] = -100;
  uinp.absfuzz[ABS_RY] = 0;
  uinp.absflat[ABS_RY] = 6;

  rc = write(fd, &uinp, sizeof(uinp));
  assert(rc == sizeof(uinp));

  rc = ioctl(fd, UI_DEV_CREATE);
  assert(!rc);
  return fd;
}

// ============================================================================
/*
 * Post keystroke down and keystroke up events:
 * (unused here but available for your own experiments)
 * dir: 1 = down, 0 = up
 * enum postkey {PKEY_UP, PKEY_DOWN};
 */
// ============================================================================
void uinput_postkey(int fd, unsigned int key, unsigned int dir) {
  struct input_event ev;
  int rc;

  memset(&ev, 0, sizeof(ev));
  ev.type = EV_KEY;
  ev.code = key;
  ev.value = dir;

  rc = write(fd, &ev, sizeof(ev));
  assert(rc == sizeof(ev));
}

// ============================================================================
/*
 * Post a synchronization point :
 */
// ============================================================================
void uinput_syn(int fd) {
  struct input_event ev;
  int rc;

  memset(&ev, 0, sizeof(ev));
  ev.type = EV_SYN;
  ev.code = SYN_REPORT;
  ev.value = 0;
  rc = write(fd, &ev, sizeof(ev));
  assert(rc == sizeof(ev));
}

// ============================================================================
/*
 * Perform uinput event for left analog pad :
 */
// ============================================================================
void uinput_left_movement(int fd, int x, int y) {
  struct input_event ev;
  int rc;

  memset(&ev, 0, sizeof(ev));
  ev.type = EV_ABS;
  ev.code = ABS_X;
  ev.value = x;

  rc = write(fd, &ev, sizeof(ev));
  assert(rc == sizeof(ev));

  ev.code = ABS_Y;
  ev.value = y;
  rc = write(fd, &ev, sizeof(ev));
  assert(rc == sizeof(ev));
}

// ============================================================================
/*
 * Perform uinput event for right analog pad :
 */
// ============================================================================
void uinput_right_movement(int fd, int x, int y) {
  struct input_event ev;
  int rc;

  memset(&ev, 0, sizeof(ev));
  ev.type = EV_ABS;
  ev.code = ABS_RX;
  ev.value = x;

  rc = write(fd, &ev, sizeof(ev));
  assert(rc == sizeof(ev));

  ev.code = ABS_RY;
  ev.value = y;
  rc = write(fd, &ev, sizeof(ev));
  assert(rc == sizeof(ev));
}

// ============================================================================
/*
 * Close uinput device :
 */
// ============================================================================
void uinput_close(int fd) {
  int rc;

  rc = ioctl(fd, UI_DEV_DESTROY);
  assert(!rc);
  close(fd);
}
