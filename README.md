# wiiclassic-jst

This project is a linux driver for the i2c wii classic controller.

# Building instructions :

With RPi:

- run raspi-config, select Advanced, enable I2C.
- install the following packages: 
  `sudo apt install libi2c-dev i2c-tools cmake build-essential`

Get the source and build:

    git clone https://bitbucket.org/zxnord/wiiclassic-jst
    mkdir wiiclassic-jst-build
    cd wiiclassic-jst-build
    cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_FLAGS=-std=c11 ../wiiclassic-jst

Run at startup:

    mv classic /usr/local/bin
    chmod 755 /usr/local/bin/classic
    sed -i "s/^exit 0/\/usr\/local\/bin\/classic \&\\nexit 0/g" /etc/rc.local >/dev/null

Also this to enable controller with game emulators:

    echo "SUBSYSTEM==\"input\", ATTRS{name}==\"classic\", ENV{ID_INPUT_KEYBOARD}=\"1\"" > /etc/udev/rules.d/10-classic.rules
